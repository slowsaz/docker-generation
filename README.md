# Dockerfile and script to manage debian/centos/arch with systemd in Docker

## Clone the project 
``` 
git clone https://gitlab.com/slowsaz/docker-generation.git
```

## Requirments Debian/Ubuntu/Mint
```
./deb-requirments.sh
```
   
## Requirments Arch/Manjaro
```
./arch-requirments.sh
```
Instead, you can install the package in package folder.   
This package use massive-containers-rm to rm containers and massive-containers-run to run containers.

## Requirments Almalinux/Fedora/RedHat
```
./almalinux-requirments.sh
```

## Add your user in docker group
``` 
gpasswd -a user group
```

## Start docker
``` 
sudo systemctl start docker
```
   
The first launch will be long. You can see the state and the result in **docker.log**.
``` 
./rundocker.sh [arg1] [arg2]
arg 1 : debian, almalinux, arch
arg 2 : number (default : 1)
```
## Kill and rm different containers
```
./rmdocker.sh [arg1] [arg2]
arg1 : debian, almalinux, arch
arg2 : number (default : 1)
```

You can create, kill and rm multiple containers with the scripts.

Before running script, you can generate your ssh key :
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

Massive-containers
Copyleft 2021  Slowsaz

>  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

