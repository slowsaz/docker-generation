#!/bin/bash
username=$(id -nu)
container=$1
numbDock=$2
if [[ -z $numbDock ]]
then
numbDock=1
fi
msg(){
	echo "Update check, please wait. This may take some time."
}
checkDock(){
	id_first=$(docker ps -a | grep $nameDock-dock* | sed s/".*-dock"//g  | sort -nr | head -1)
	id=$(($id_first+1))
	echo "=> Container ${username}-${nameDock}-dock${id}"
}
sshkey(){
	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "useradd -m -p sa3tHJ3/KuYvI ${username}"
	echo "Public key installation ${HOME}/.ssh/id_rsa.pub"
	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "mkdir  ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown ${username}:${username} $HOME/.ssh"
	docker cp ${HOME}/.ssh/id_rsa.pub ${username}-${nameDock}-dock${id}:${HOME}/.ssh/authorized_keys
	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown ${username}:${username} ${HOME}/.ssh/authorized_keys"
	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "echo '${username}   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
}
helping(){
    echo "./rundocker.sh [arg1] [arg2 (optional)]
	arg 1 : debian, almalinux, arch
	arg 2 : number (default : 1)"
}
resume(){
	for i in $(docker ps -a --format "{{ .Names }}" |grep "dock" );do
		infos_conteneur=$(docker inspect -f '   => {{.Name}} - {{.NetworkSettings.IPAddress }}' ${i})
		echo "${infos_conteneur} - Utilisteur : ${username} / mdp:password"
	done
}
container()
{
	case $container in
	debian)
		nameDock=$container
		msg
		#./make.sh
		for (( c=1; c<=$numbDock; c++ ));do
		echo "Creation of debian container"
		checkDock
		docker run -tid --publish-all=true -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name ${username}-${nameDock}-dock${id} -h ${username}-${nameDock}-dock${id} local/debian-systemd > /dev/null 2>&1
		sshkey > /dev/null 2>&1
		docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "service ssh start"
		done
		resume;;
	almalinux)
		nameDock=$container
		msg
		#./make.sh
		for (( c=1; c<=$numbDock; c++ ));do
		echo "Creation of Almalinux container"
		checkDock
		docker run -tid --publish-all=true -v /sys/fs/cgroup:/sys/fs/cgroup:ro --privileged --name ${username}-${nameDock}-dock${id} local/almalinux-systemd > /dev/null 2>&1
		sshkey > /dev/null 2>&1
		docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "/usr/sbin/sshd"
		docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "rm -rf /var/run/nologin"
		done
		resume;;
	#lamp)
	#	nameDock=$container
	#	msg
		#./make.sh
	#	for (( c=1; c<=$numbDock; c++ ));do
	#	echo "Creation of debian LAMP container"
	#	checkDock
	#	docker run -tid --publish-all=true -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name ${username}-${nameDock}-dock${id} -h ${username}-${nameDock}-dock${id} local/debian-systemd-lamp > /dev/null 2>&1
	#	sshkey > /dev/null 2>&1
	#	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "service ssh start"
	#	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "service apache2 start"
	#	docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "service mysql start"
	#	done
	#	resume;;
	arch)
		nameDock=$container
		msg
		#./make.sh
		for (( c=1; c<=$numbDock; c++ ));do
		echo "Creation of arch container"
		checkDock
		docker run -tid --publish-all=true -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name ${username}-${nameDock}-dock${id} -h ${username}-${nameDock}-dock${id} local/arch > /dev/null 2>&1
		sshkey > /dev/null 2>&1
		docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "/usr/bin/ssh-keygen -A" > /dev/null 2>&1
		docker exec -ti ${username}-${nameDock}-dock${id} /bin/bash -c "/usr/sbin/sshd"
		done
		resume;;
	--help)
		helping;;
	*)
		helping;;
esac
}
container
