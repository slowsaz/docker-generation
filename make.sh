#!/bin/bash
pull(){
docker pull debian
docker pull almalinux/almalinux
docker pull archlinux
}
#&& cd ../debianLaravel && docker build --rm -t local/debian-systemd-lamp .
build(){
cd almalinux/ && docker build --rm -t local/almalinux-systemd . && cd ../debian/ && docker build --rm -t local/debian-systemd . && cd ../arch/ && docker build --rm -t local/arch . && cd .. && source ./docker-generation-completion.sh
}
pull > ./docker.log 2>&1
build >> ./docker.log 2>&1
