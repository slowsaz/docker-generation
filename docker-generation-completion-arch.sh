_completion-run()
{

	local cur prev opts opts1
	COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="arch debian almalinux--help"
    opts1="number"
	COMPREPLY=($(compgen -W "${opts}" -- ${cur}))

	return 0
}
complete -F _completion-run massive-containers-run

_completion-rm()
{

	local cur prev opts
	COMPREPLY=()
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="arch debian almalinux--help"
	COMPREPLY=($(compgen -W "${opts}" -- ${cur}))

	return 0
}
complete -F _completion-rm massive-containers-rm
